#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "func.h"

double average (int list[], int n){
	double sum = 0;
	for (int i=0; i<8; i++){
		sum = sum + list[i];
	}
	return sum/n;
}

double median (int list[], int n){
	bool even = (n%2==0);
	
	if (even){
		return ((list[n/2-1] + list[n/2])/2.0);
	}
	else{
		
		return (list[(n/2)]);
	}
	
}
