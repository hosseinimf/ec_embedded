#include <stdio.h>

int main() {
    
    char sos[]=". . . _ _ _ . . .";   // Define and initialize the string
    
    // Use while loop and set 1 for an infinite loop
    while(1){
        printf("%s\n", sos);    // Print out the morse
    }

    return 0;
}
