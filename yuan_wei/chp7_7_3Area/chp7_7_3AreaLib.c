#include<math.h>
#include"chp7_7_3AreaLib.h"
#define PI 3.14

double rectangle(double len, double wid)
{
	double area = len*wid; 
	
	return area;
}

double triangle(double a, double b, double c)
{
   double s, area;
   
   s = (a+b+c)/2;
   
   area = sqrt(s*(s-a)*(s-b)*(s-c));
   
   return area;
}

double circle(double r)
{
	double area;

	area = PI*2*r;

	return area;
}


 
