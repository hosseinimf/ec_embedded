#include <stdio.h>
#include <string.h>
#include <ctype.h>


int main() {

  //Print and create file "minfil" with text in rövarspråk
  FILE *fp1 = fopen("minfil.txt", "w");

  fprintf(fp1, "Hohejoj! Jojagog hohetoteror Momalolinon.");
  fclose(fp1);

  //Open "minfil"
  FILE *fp2 = fopen("minfil.txt", "r"); 
  
  char cons[] = "bcdfghjklmnpqrstvwxz"; //Declare consonants in array
  int c;
  
  //While loop that translate rövarspråk to regular text
  while ((c = fgetc(fp2)) != EOF) {
    putchar(c);
    if (strchr(cons, tolower(c)) != NULL) {
        fgetc(fp2); fgetc(fp2);
    }
  }
  fclose(fp2);
  return 0;
}




