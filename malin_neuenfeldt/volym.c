#include <stdio.h>

int main (){
//Deklarerar i som int variabel
int i;
//Skriver ut nummer 1 till 12 där varje rad också visar talet i kvadrat samt talet i kubik
for (i=1; i<=12; i+=1){
	printf("Tal %d Kvadrat = %d Kubik = %d\n", i, i*i, i*i*i);
}
return 0;
}
