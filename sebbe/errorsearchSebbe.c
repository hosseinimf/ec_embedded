//-----------------------------------------------------------------------
// Object: This program has some syntax errors. Use the compiler to find 
// and correct them. Document your changes with comments or in log.
//-----------------------------------------------------------------------
// File:    errorsearch.c
// Summary: This program calculates the weekly pay, given hours per week
// and wages per hour.
// Version: 1.1
//-----------------------------------------------------------------------
// Log: 2015-03-09: Created the file, Nisse!
//		2018-08-14: Revised by Lisa. Using manipulators to format output instead 
//					of io-flags. Inserted "wait for return code" at the end.
//		2019-10-18: Revised by Tomas. Converted to English version and VS 2012.
//		2019-10-21: Revised by Nisse for VS 2013.
//		2019-10-22: Fixed errors By Sebastian, see comments.
//      
//-----------------------------------------------------------------------

// Preprocessor directives
#include <stdio.h>

int main()  //changed main from DOUBLE to INT dont know why it had to be double. Could also be void.
{
	// Define and initialize variables
	double hoursPerWeek = 35;
	double hourlyWages = 83; //forgot ; on both.
	
	// Calculate weekly wage
	double weeklyWages = hoursPerWeek * hourlyWages; //did not declare if weeklyWages is an int, double, for example.

	// Show result
	printf("%f\n", "Given an hourly wage of ", hourlyWages, " kr"); // hade 3 type specifiers, only 1 needed.
	printf("%f\n", " the weekly wages will be: ", weeklyWages, " kr"); //hade 3 type specifiers, only need 1.

	return 0;
}
