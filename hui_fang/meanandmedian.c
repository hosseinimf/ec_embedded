#include <stdio.h>
#define N 10
#define M 2

float* mean(float * arrp);
void median(float * arrp);
void sort(int n, float* ptr);

static float meanArray[2];	//Global static arrary to get the means

int main()
{

	float grades[2][10];
	int select;
	float * ptr;
	char *iot="IoT grades";

		// Get IoT and CAD credits and store them in the credit array
	for (int i=0; i<2;i++){
		for (int j=0; j<10;j++){
			printf("%s for student %d: ", iot, j+1);
			scanf("%f", &grades[i][j]);
		}
		iot="CAD grades";
	}

	printf("\n");

	// call calMean function to calculate the means
	mean(grades);
	printf("**********************************\n");
	printf("IoT mean: %5.2f    CAD mean: %5.2f\n", meanArray[0], meanArray[1]);

	// Call the calMedian function to calculate the meaidans
	median(grades);
	printf("\n");


	return(0);
}

//mean function takes an array and outputs a 1-D array of means
float* mean(float * arrp)
{
	 float resultIn;


	 for (int i=0; i<2; i++)
	 {
		 for(int j=0; j < 10; j++)
	     {
			 resultIn += *arrp++;
	     }
		 meanArray[i]=resultIn/10.0;

	     resultIn=0.0;
	 }

	 return meanArray;
}

//median takes an array pointer and returns an array of calculate medians
 
void median(float * arrp) {

    float temp[N];
    int i, j;

    for(i=0; i<M; i++)
    {
        for(j=0; j<N; j++)
        {
            temp[j] = *arrp++;
        }

    sort(N, temp);

    if(N%2 ==0)
    {
        printf("  median: %5.2f    ", (temp[(N/2 - 1)] + temp[(N/2)])/2);
    }
    else
    {
        printf("  median: %5.2f    ", temp[(N/2)]);
    }

    }
}

//sort funcion takes 1-D array and return sorted array

void sort(int n, float* ptr)
{
    int i, j, t;
    
    for (i = 0; i < n; i++) {

        for (j = i + 1; j < n; j++) {

            if (*(ptr + j) < *(ptr + i)) {

                t = *(ptr + i);
                *(ptr + i) = *(ptr + j);
                *(ptr + j) = t;
            }
        }
    }
}
