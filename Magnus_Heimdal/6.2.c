#include<stdio.h>
#include<stdlib.h>

/*
En funktion som beräknar vad en vara kostar inklusive moms. Som inparametrar tar funktionen emot priset exklusive moms 
samt momsen i procent
*/
double finalPrice(double exclusive,double procentage)
{
    return exclusive + exclusive*procentage/100;
}

int main () 
{

double price,VAT = 0;
printf("Enter Price w/o VAT \n");
scanf("%lf",&price);

printf("Enter VAT \n");
scanf("%lf",&VAT);

printf("Price including VAT:%.2f",finalPrice(price,VAT));

    return 0;
}