#include "functions.h"

void print_table(int table[][COL])
{
   char average[] = "Medelvärde";
   char median[] = "Median";

   for (int i = 0; i < ROW; i++)
   {
      printf("\tKlass #%d", i + 1);
   }
   puts("");

   // Print values
   for (int i = 0; i < COL; i++)
   {
      // Print values
      for (int j = 0; j < ROW; j++)
      {
      printf("\t%8d", table[j][i]);
      }
      puts("");
   }
}

double average(int array[], int length)
{
   double sum = 0;
   for (int i = 0; i < length; i++)
   {
      sum += array[i];
   }
   return sum / length;
}

double median(int array[], int length)
{
   double median;
   // If length is a even number
   if (length % 2 == 0)
   {
      int x = length/2;
      median = (array[x-1] + array[x]) / 2.0;
   }
   else
   {
      median = array[length%2+1];
   }
   return median;   
}
