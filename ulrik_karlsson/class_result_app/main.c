#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

void main()
{
   system("chcp 1252");
   system("cls");

   int results[ROW][COL];

   // Scann results from all classes
   for (int i = 0; i < ROW; i++)
   {
      printf("Ange alla resultat (i storleksordning) f�r klass #%d:\n", i + 1);
      for (int j = 0; j < COL; j++)
      {
         scanf("%d", &results[i][j]);
      }
   }

   print_table(results);
   printf("\nMedelv�rde %5.1f \t%8.1f", average(results[0], COL), average(results[1], COL));
   printf("\nMedian %9.1f \t%8.1f", median(results[0], COL), median(results[1], COL));
}
