#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void draw_led(){
  puts("\n");
  puts("         :      ");
  puts("     '.  _  .'  ");
  puts("    -=  (~)  =- ");
  puts("     .'  #  '.  ");
  puts("\n");
}

void draw_blank(){
  puts("\n\n\n\n\n\n\n");
}

// this function will loop doing nothing for given number of  milli secounds
void delay( int milli_secounds){
  // Storing current time to use as reference
  clock_t start_time = clock();

  // Looping till clock catches up with start time + given number of milli secounds
  while (clock() < start_time + milli_secounds);
}

int main(){
  system("chcp 1252");
  system("cls");
  // this is the instructions for the LED 1=on 0=off
  char instructions[] = "101010111011101110101010000";

  while(1){
    for (int i = 0; i < strlen(instructions); i++){
      system("cls");
      // draw a LED if instruction is 1
      if (instructions[i] == '1'){
        draw_led();
      }
      else{
        draw_blank();
      }
      delay(200);
    }
  }
  return 0;
}
