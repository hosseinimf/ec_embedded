#include "translate.h"

void translate_to_rovarsprak(const char* string)
{
   int nextPosition = 0;
   // Go through each character in string
   for (int i = 0; i < strlen(string); i++)
   {
      // Add current character to translated
      translated[nextPosition++] = string[i];
      // Save current character in lowercase
      char current_char = tolower(string[i]);
      // See if current character is a consonant
      if (strchr(CONSONANTS, current_char))
      {
         translated[nextPosition++] = 'o';
         translated[nextPosition++] = current_char;
      }
   }
   translated[nextPosition] = '\0';
}

void translate_from_rovarsprak(const char* string)
{
   int nextPosition = 0;
   // Go through each character in string
   for (int i = 0; i < strlen(string); i++)
   {
      // Add current character to translated
      translated[nextPosition++] = string[i];
      // Save current character in lowercase
      char current_char = tolower(string[i]);
      // See if current character is a consonant
      if (strchr(CONSONANTS, current_char))
      {
         i += 2;
      }
   }
   translated[nextPosition] = '\0';
}