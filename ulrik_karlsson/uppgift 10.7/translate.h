#include <string.h>
#include <ctype.h>
#define CONSONANTS "bcdfghjklmnpqrstvwxyz"
#define LENGTH 1000

char translated[LENGTH];

void translate_to_rovarsprak(const char* string);
void translate_from_rovarsprak(const char* string);