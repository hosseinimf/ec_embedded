/*_________________________________Programmeringsuppgifter_______________________________|
|                Created by Walaeldin Abdoon Saeed Abdoon, WASA                          |
|                        Date 18 october 2019                                            |
| OBS:  the Header file "9.h" found in the same path.                                |
| This C program file contain a multiple functions for studant & course operations       |
| Function prototypes :                                                                  |
|                                                                                        |
| students operations function definions:                                                |
|                                                                                        |
|   float maximumpoints(int course,int students, int points[students][course])                         |
|   float minimumpoints(int course,int students, int points[students][course])                         |
|   float average(int course, int students, int points[students][course])                              |
|   float median(int course,int students, int points[students][course])                                |
|   void printing(int course,int students, int points[students][course])                               |
|_______________________________________________________________________________________*/

                                                                                        
   void maximumpoints(int course,int students, int points[students][course]);                         
   void minimumpoints(int course,int students, int points[students][course]);                         
   float average(int course, int students, int points[students][course]);                              
   float median(float Smax, float Smin);                               
   void printing(int course,int students, int points[students][course]);                               
   void generatermatrices(int course,int students, int points[students][course]);