#include <string.h>





// function that sorts array in order low to high
static void sortArray(double array[],int size){
    
    //every loop 2nd last element becames last element  
    for (int i = size-1; i > 0; i--)
    {
        //checks if the elements is bigger than next element
        for (int j = 0; j < i; j++)
        {
            if (array[j]>array[j+1])
            {
                double temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
        
    }

}


// function that finds mode of the array that is sorted or not, this function does not change the orginal array 
double median(double array[],int size){

    // copy array
    double tempArray[size];
    memcpy(tempArray,array, sizeof tempArray);

    // sort the array
    sortArray(tempArray,size);

    // check if the total elements is odd or even
    if (size%2==0)
    {
        return (tempArray[size/2] + tempArray[size/2-1])/2;
    } else
    {
        int n = size/2+0.5;
        return tempArray[n];
    }

}

// gets the average of the array
double average(double array[],int size){
    double sum = 0;
    for (int i = 0; i < size; i++)
    {
        sum += array[i];
    }

    return sum/size;
}