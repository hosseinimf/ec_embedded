//include areaf.h and stdio in you main file
//define functions that outputs/returns area values
#include <stdio.h>
#include "areaf.h"

//get requird info and calculate area of the shape
double circleArea(){
    printf("enter raide for circle to get area value: ");
    double r;
    scanf("%lf",&r);
    return r*r*PI;
}

double rectangelArea(){
    printf("enter width and then height to get area value of the rectangle: ");
    double w;
    double h;
    scanf("%lf%lf",&w,&h);
    return w*h;
}

double triangelArea(){
    printf("enter width and then height to get area value of the triangle: ");
    double w;
    double h;
    scanf("%lf%lf",&w,&h);
    return (w*h)/2;
}