#include<stdio.h>
#include "funchead.h"

int main()
{
    // Hardcoded numbers for student result.
    // Column 1(index0) IOT, all students points of test  
    // Column 2(index1) CAD, all students points of test
    // Column 3(index2) SERVER, all students points of test
    
    // Rows is iot,cad,server points present in 10 rows.
    int choice;    
    int resultList[10][3] ={{12,20,18},
                            {22,13,12},
                            {18,17,14},
                            {20,12,17},
                            {14,23,19},
                            {12,11,10},
                            {24,24,24},
                            {16,15,19},
                            {19,18,13},
                            {21,12,16}};
    
    while(1)
    {   //Menu for viewing points by chooseing iot,cad or server course or other to quit
        printf("View result for Iot Push 1, Cad Push 2, Server Push 3 or other to quit: ");
        if (scanf("%d",&choice) != 1)
        {
            printf("\nYou choose to quit\n");
            break;
        }
        //IOT course
        if (choice == 1)
        {    
            printf("\n");
            char str[] = "IOT";
            printresult(str, 0, resultList);
            printf("\n");
            printf("Average of %s students is %0.2f points.\n",str,average(0,resultList)); 
            printf("\n");
            median(0,resultList);
            printf("\n");
        }//CAD course
        else if (choice == 2)
        {
            printf("\n");
            char str[] = "CAD";
            printresult(str, 1, resultList);
            printf("\n");
            printf("Average of %s students is %0.2f points.\n",str,average(1,resultList)); 
            printf("\n");
            median(1,resultList);
            printf("\n");
        }
        else
        {//SERVER course
            printf("\n");
            char str[] = "SERVER";
            printresult(str, 2, resultList);
            printf("\n");
            printf("Average of %s students is %0.2f points.\n",str,average(2,resultList)); 
            printf("\n");
            median(2,resultList);
            printf("\n");
        }  
    }   
}