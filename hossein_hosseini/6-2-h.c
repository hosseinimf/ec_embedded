#include <stdio.h>
#include <stdlib.h>
#include <math.h>
double inkludemoms (double pris, double momsprocent);       // funktion prototype

int main()                  // uppgift 6-2  Hossein Hosseini
{
    double p_utanmoms, p_medmoms, moms, momsprocent;     // defeniera variablar
    printf("\nskriv priset exklusive moms\n");
    scanf("%lf",&p_utanmoms);                            // pris utan moms

    printf("\nskriv momssatsen i procent\n");
    scanf("%lf",&momsprocent);                            // momssatsen i procent

    p_medmoms = inkludemoms (p_utanmoms,momsprocent);     // anropa funktionen

    printf("\npriset inklude moms blir    %lf\n", p_medmoms);
    return (0);
}




double inkludemoms (double pris, double momsprocent)            // defeniera funktionen
{
    double inkmoms;
    inkmoms = pris * ((momsprocent/100)+1);
    return (inkmoms);
}
