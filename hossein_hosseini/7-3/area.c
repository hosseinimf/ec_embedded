// funktionsbib area
#include "area.h"
#include <math.h>
#define PI 3.14


double cirkelarea (double r)
{
    return  (PI*r*r);
}

double rektangelarea (double l, double w)
{
    return  (l*w);
}

double triangelarea (double h , double b)
{
    return  (h*b/2);
}

