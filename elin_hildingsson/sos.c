#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>


int main ()
{
	//bool med namnet run som är sant.
	bool run = true;

	//så länge run är sant körs loopen.
	while (run)
	{
		//rensar skärmen.
		system("cls");

		//skriver ut samt pausar olika länge beroende på lång eller kort utskrift = s.o.s
		sleep(1);
		printf(".");
		sleep(1);
		printf(".");
		sleep(1);
		printf(".");
		sleep(1);
		printf("_");
		sleep(3);
		printf(" _");
		sleep(3);
		printf(" _");
		sleep(3);
		printf(".");
		sleep(1);
		printf(".");
		sleep(1);
		printf(".");
		sleep(5);
	}
	return 0;
}
