#include <stdio.h>
#include <string.h>  //to be able to use strchr
#include <ctype.h>   //to be able to use tolower
#include <locale.h>  //to be able to use setlocale

//main function were the program starts
int main() {
  //to be able to use å ä ö in the text.
  setlocale(LC_ALL, ""); 
  //reads the file minfil.txt with the robber language.
  FILE *file = fopen("minfil.txt", "r"); 
  char consonants[] = "bcdfghjklmnpqrstvwxz";
  
  //if fopen fails it gets value NULL.
  if(file == NULL)
  {
  	printf("\nfile could not be found\n");
  }

  int c;
  printf("\nThis is what the file says:\n\n");
  //while-loop that translates the robber language.
  while ((c = fgetc(file)) != EOF)
  {
    putchar(c);
    if (strchr(consonants, tolower(c)) != NULL) 
    {
        fgetc(file); fgetc(file);
    }
  }
  printf("\n");
  fclose(file);
  return 0;
}