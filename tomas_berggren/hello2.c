#include <stdio.h>

//function to print out Hello, World
void print() {
// printf() displays the string inside quotation
   printf("Hello, World!");
}

//main function to start the program
int main()
{
	print(); 
	print();  
   return 0;
}