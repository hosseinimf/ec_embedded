/*Program som tar fram medelv�rde och median p� f�best�mda provresultat*/
#include <stdio.h>

double medelv(double a[][2], int n); //deklaration av funktion som returnerar medelv�rdet av en kolumn
double median(double a[][2], int n); //deklaration av funktion som returnerar medianen av en kolumn

int main()
{
    double resultat[10][2] = {{43, 37},  //f�rbest�mda provresultat, kolumn 1 = klass 1
                          {24.5, 26},    //kolumn 2 = klass 2
                          {30, 16},
                          {28, 29},
                          {43, 42.5},
                          {24, 38},
                          {30, 34},
                          {28, 29.5},
                          {43, 42},
                          {24, 38}};
    int svar;
    double klass;

    while(1) //loop som inte bryts f�rens anv�ndaren v�ljer att avsluta.
    {               
    printf("Ange 1 f�r att visa resultat f�r klass 1.\nAnge 2 f�r att visa resultat f�r klass 2.\nEller ange eof f�r att avsluta.\nAnge: ");

        if(scanf("%d", &svar) != 1) //Om anv�ndaren anger "eof", avslutas programmet
        {
            break;
        }
        if(svar == 1)  //Om anv�ndaren anger "1", presenteras medelv�rde och median f�r klass 1
        {
            klass = 0;
            printf("Klass 1: Medelv�rde = %.2f\n\tMedian = %.2f\n\n", medelv(resultat, klass), median(resultat, klass));
        }
        else if(svar == 2) //Om anv�ndaren anger "2", presenteras medelv�rde och median f�r klass 2
        {
            klass = 1;
            printf("Klass 2: Medelv�rde = %.2f\n\tMedian = %.2f\n\n", medelv(resultat, klass), median(resultat, klass));   
        }
    }
}
double medelv(double a[][2], int n) //Funktions definition av medelv
{
    double summa = 0;
        for(int i = 0; i < 10; i++)
        {
            summa = summa + a[i][n];
        }
    return summa / 10;
}
double median(double a[][2], int n)  //Funktions definition av median
{
    double byte = 0;
    for(int i = 0; i < 10; i++)
    {
        for(int j = i + 1; j < 10; j++)
        {
            if(a[i][n] > a[j][n])
            {
                byte = a[j][n];
                a[j][n] = a[i][n];
                a[i][n] = byte;
            }
        }
    }
    return (a[4][n] + a[5][n]) / 2;
}